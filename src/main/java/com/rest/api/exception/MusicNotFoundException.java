package com.rest.api.exception;

public class MusicNotFoundException extends Exception {
    private long music_id;
    public MusicNotFoundException(long music_id)  {
        super(String.format("Music is not found with id : '%s'", music_id));
    }
}
