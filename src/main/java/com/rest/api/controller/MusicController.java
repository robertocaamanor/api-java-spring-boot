package com.rest.api.controller;
import com.rest.api.model.Music;
import com.rest.api.service.FileService;
import com.rest.api.exception.MusicNotFoundException;
import com.rest.api.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@CrossOrigin
@RestController
public class MusicController {
    @Autowired
    MusicRepository musicRepository;

    @GetMapping("/music")
    public List<Music> getAllNotes() {
        return musicRepository.findAll();
    }

    @PostMapping("/music")
    public Music createNote(@Valid @RequestBody Music music) {
        return musicRepository.save(music);
    }

    @GetMapping("/music/{id}")
    public Music musicRepository(@PathVariable(value = "id") Long musicId) throws MusicNotFoundException {
        return musicRepository.findById(musicId)
                .orElseThrow(() -> new MusicNotFoundException(musicId));
    }

    @PutMapping("/music/{id}")
    public Music updateNote(@PathVariable(value = "id") Long musicId,
                           @Valid @RequestBody Music musicDetails) throws MusicNotFoundException {

        Music music = musicRepository.findById(musicId)
                .orElseThrow(() -> new MusicNotFoundException(musicId));

        music.setAlbum_name(musicDetails.getAlbum_name());
        music.setArtist_name(musicDetails.getArtist_name());
        music.setGenre(musicDetails.getGenre());
        music.setLabel(musicDetails.getLabel());
        music.setPrize(musicDetails.getPrize());
        music.setStock(musicDetails.getStock());

        Music updatedMusic = musicRepository.save(music);

        return updatedMusic;
    }

    @DeleteMapping("/music/{id}")
    public ResponseEntity<?> deleteMusic(@PathVariable(value = "id") Long musicId) throws MusicNotFoundException {
        Music music = musicRepository.findById(musicId)
                .orElseThrow(() -> new MusicNotFoundException(musicId));

        musicRepository.delete(music);

        return ResponseEntity.ok().build();
    }

}
