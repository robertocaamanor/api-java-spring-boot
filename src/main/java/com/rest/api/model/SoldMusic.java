package com.rest.api.model;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
@Entity
@Table(name = "sold_music")
public class SoldMusic {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private Integer album_id;
    @NotBlank
    private Integer quantity;
    @NotBlank
    private Integer sold_prize;
    public SoldMusic(){
        super();
    }
    public SoldMusic(Long id, Integer album_id, Integer quantity, Integer sold_prize) {
        super();
        this.id = id;
        this.album_id = album_id;
        this.quantity = quantity;
        this.sold_prize = sold_prize;
    }
    public Long getId(){
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getAlbum_id(){
        return album_id;
    }
    public void setAlbum_id(Integer album_id){
        this.album_id = album_id;
    }
    public Integer getQuantity(){
        return quantity;
    }
    public void setQuantity(Integer quantity){
        this.quantity = quantity;
    }
    public Integer getSold_prize(){
        return sold_prize;
    }
    public void setSold_prize(Integer sold_prize){
        this.sold_prize = sold_prize;
    }
}
