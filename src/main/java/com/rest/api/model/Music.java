package com.rest.api.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@Entity
@Table(name = "music")
public class Music {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String album_name;
    @NotBlank
    private String artist_name;
    @NotBlank
    private String label;
    @NotBlank
    private String genre;
    @NotNull
    private Integer prize;
    @NotNull
    private Integer stock;
    private String file;
    public Music(){
        super();
    }
    public Music(Long id, String album_name, String artist_name, String label, String genre, Integer prize, Integer stock, String file) {
        super();
        this.id = id;
        this.album_name = album_name;
        this.artist_name = artist_name;
        this.label = label;
        this.genre=genre;
        this.prize = prize;
        this.stock = stock;
        this.file = file;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getAlbum_name() {
        return album_name;
    }
    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }
    public String getArtist_name() {
        return artist_name;
    }
    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getGenre(){
        return genre;
    }
    public void setGenre(String genre){
        this.genre = genre;
    }
    public Integer getPrize(){
        return prize;
    }
    public void setPrize(Integer prize){
        this.prize = prize;
    }
    public Integer getStock(){
        return stock;
    }
    public void setStock(Integer stock){
        this.stock = stock;
    }
    public String getFile(){
        return file;
    }
    public void setFile(String file){
        this.file = file;
    }
}
