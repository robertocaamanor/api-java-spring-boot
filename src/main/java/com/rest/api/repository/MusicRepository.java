package com.rest.api.repository;
import com.rest.api.model.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface MusicRepository extends JpaRepository<Music, Long> {
}